import java.util.Scanner;
public class Shop{
  public static void main (String[]args){
    Scanner reader = new Scanner(System.in);
    
    Pets[] animals = new Pets[4];
    for (int i = 0; i < 4; i++) {
      animals[i] = new Pets();
      System.out.println("Enter the name of the pet");
      animals[i].name = reader.next();
      System.out.println("Enter the classification of the pet (mammal? reptile? etc)");
      animals[i].classification = reader.next();
      System.out.println("Enter the lifespan of the pet");
      animals[i].lifespan = reader.nextInt();
    }
    System.out.println(animals[3].name);
    System.out.println(animals[3].classification);
    System.out.println(animals[3].lifespan);
    animals[3].coolness();
  }
}
